# big-brother-v2

### Notes ###
* [babel-cli](https://babeljs.io/docs/usage/cli/) should not be used in production because of heavy resource usage.
** [babel](https://babeljs.io/) allows use to use ES6 through syntax transformers since it isn't yet supported by Node.js natively. 
** The code will instead need to be precompiled and the resulting build served.
