import express from 'express';
// can import middlewares or models

const router = express.Router();

router.get('/test', (req, res) => {
	res.send('test');
});

module.exports = router;