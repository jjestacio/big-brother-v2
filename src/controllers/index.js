import express from 'express';

const router = express.Router();

router.use('/google', require('./google'));
router.use('/messenger', require('./messenger'));

module.exports = router;