import express from 'express';
import bot from 'models/bot';

const router = express.Router();

// TODO: move challenge into a middleware
router.post('/webhook', (req, res) => {
	// Parse the request body from the POST
	const body = req.body;

	body.entry.forEach(function (entry) {
		const webhookEvent = entry.messaging[0];
		const senderPSID = webhookEvent.sender.id;
	
		if (webhookEvent.message) {
			console.log("Message");
			bot._handleMessage(body);
			res.sendStatus(200);
		} else if (webhookEvent.postback) {
			console.log("Postback");
			bot._handleMessage(body);
			res.sendStatus(200);
		}
	});

});

router.get('/webhook', (req, res) => {
	// Your verify token. Should be a random string.
	const VERIFY_TOKEN = process.env.SECRET_VERIFY_TOKEN;

	// Parse the query params
	const mode = req.query['hub.mode'];
	const token = req.query['hub.verify_token'];
	const challenge = req.query['hub.challenge'];

	// bot._verify(req, res);

	// Checks if a token and mode is in the query string of the request
	if (mode && token) {
		// Checks the mode and token sent is correct
		if (mode === 'subscribe' && token === VERIFY_TOKEN) {
			// Responds with the challenge token from the request
			console.log('WEBHOOK_VERIFIED');
			res.status(200).send(challenge);

		} else {
			// Responds with '403 Forbidden' if verify tokens do not match
			res.sendStatus(403);
		}      
	}
});

module.exports = router;