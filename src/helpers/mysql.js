import mysql from 'mysql';

const connection = mysql.createConnection({
	host     : process.env.MYSQL_HOST, 
 	user     : process.env.MYSQL_USER,
	port     : process.env.MYSQL_PORT,
 	password : process.env.MYSQL_PASSWORD,
 	database : process.env.MYSQL_DATABASE,
	// debug: true,
});

connection.connect(err => console.log( err ? err : "Connected to connection" ));

module.exports = connection