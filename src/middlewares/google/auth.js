import google from 'googleapis';

const creds = require('./client_secret.json');

const oauth2Client = new google.auth.OAuth2(
  creds.web.client_id,
  creds.web.client_secret
);

// generate a url that asks permissions for Google+ and Google Calendar scopes
const scopes = [
  'https://www.googleapis.com/auth/drive',
  'https://www.googleapis.com/auth/calendar',
];

const url = oauth2Client.generateAuthUrl({
  // 'online' (default) or 'offline' (gets refresh_token)
  access_type: 'offline',

  // If you only need one scope you can pass it as a string
  scope: scopes
});

module.exports = (req, res, next) => {
	
	next();
};
