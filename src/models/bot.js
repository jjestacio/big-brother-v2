import Bot from 'messenger-bot';
import Brother from 'models/brother';

const bot = new Bot({
  token: process.env.PAGE_ACCESS_TOKEN,
  verify: process.env.SECRET_VERIFY_TOKEN,
  app_secret: process.env.APP_SECRET,
});

bot.on('error', error => {
  console.log(error);
});

// TODO: pass in function for reply to process message
bot.on('message', (payload, reply) => {
  const fbId = payload.sender.id;

  bot.getProfile(fbId, (err, profile) => {
    if (err) 
      throw err;

    console.log("getProfile");
    let brother;
    
    Brother.getBrother(fbId).then(brother => brother = brother);
    console.log(brother);
    if (!brother) {
      console.log("HERE");
      Brother.addBrother({ ...profile, fb_id: fbId })
        .then(success => {
          brother = Brother.getBrother(fbId);

          const GENDER_PREFIX = brother.gender == "male" ? "Mr." : "Ms.";
          const INTRO_MESSAGE = `Hi I'm Big Brother, nice to meet you ${GENDER_PREFIX} ${brother.first_name} ${brother.last_name}.`;
  
          reply({ text: INTRO_MESSAGE }, err => {
            if (err)
              throw err;
          });
        });
    }
  });
});

module.exports = bot;