import db from 'models/database';

// TODO: migrate to new db model
class Brother {
	constructor() {}
	
	// TODO: 
	static async addBrother(fbProfile) {
		const { fb_id, first_name, last_name, gender } = fbProfile;
		const info = { fb_id, first_name, last_name, gender };

		let success;

		await db.query('INSERT INTO brothers SET ?', info)
		  		.then(results => success = true)
		  		.catch(err => success = err);

		return success;
	}
	
	// TODO: named arguments
	static async getBrother(fbId) {
		let brother;
		
		await db.query(`SELECT * FROM brothers WHERE fb_id = ${fbId};`)
				.then(results => brother = results[0])
				.catch(err => brother = err);
		
		return brother;
	}
}

module.exports = Brother;