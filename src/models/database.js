import connection from 'helpers/mysql';

class Database {
	constructor() {}

	query( sql, args ) {
        return new Promise(( resolve, reject ) => {
            connection.query( sql, args, ( err, rows ) => {
                if ( err )
                    return reject( err );
                resolve( rows );
            } );
        } );
    }

    close() {
        return new Promise( ( resolve, reject ) => {
            connection.end( err => {
                if ( err )
                    return reject( err );
                resolve();
            } );
        } );
    }
};

const db = new Database();

module.exports = db